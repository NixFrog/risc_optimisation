#include "Sobel.hpp"

Sobel::Sobel(){}

int Sobel::sobelMatrixApplication_opti(int i00, int i01, int i02, int i10, int i12, int i20, int i21, int i22){
	int isave = i02 - i20;
	int H = -i00 + isave - (i10<<1) + (i12<<1) + i22;
	int V = i00 + (i01<<1) + isave - (i21<<1) - i22;
	return (abs(H) + abs(V) < 255 ? abs(H) + abs(V) : 255);
}


void Sobel::sobelFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl){
	unsigned char *im_in = (unsigned char*)(frame_gray.data);
	unsigned char *im_out = (unsigned char*)(frame_sobel.data);
	
	int max = rw*(cl-2) -2;
	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];

	int rw_double = rw<<1;
	int rw_double_sum_one = rw_double +1;
	int rw_sum_two = rw + 2;
	int rw_double_sum_two = rw_double +2;
	
	// #pragma omp parallel for
	for (int i = 0 ; i < max; i+=8) {
		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication_opti(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
	}
}
