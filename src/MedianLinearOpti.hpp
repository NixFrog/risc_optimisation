#pragma once

#include <iostream>
#include <cmath>
#include <string>

#include "cv.h"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

// types definitions
typedef unsigned char uint8;

#define SWAP(a, b) { if ((a)>(b)) std::swap((a),(b)); }
#define P 256
#define NTHREADS 4

struct threadDataLimits
{
	int xi;
	int yi;
	int xf;
	int yf;
};

class MedianLinearOpti {
private:
	int **tab_;


public:
	MedianLinearOpti(int C);
	~MedianLinearOpti();
	void medianFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int L, int C, int R);
	
private:
	int **alloc(int L, int C);	
	void inline copy(int **tabA, int L, int C, int **tabB);
	void freeH(int **tab);
	int medianOfHisto(int *histo, int sizeOfElem);
	void removeHisto(int *histo);
	void addHisto(int *histo);
	void printHisto(int *histo);
	
private:
	int **histos_;
	int *currentHisto_;
};