#include "MedianLinear.hpp"

MedianLinear::MedianLinear(int C, int P){
	histos_ = alloc(C, P);
	currentHisto_ = (int *)malloc(sizeof(int)*P);
}

MedianLinear::~MedianLinear(){
	freeH(histos_);
	free(currentHisto_);
}

int ** MedianLinear::alloc(int nbLin, int nbCol){
	int **tab = (int **)malloc(sizeof(int*)*nbLin);
	int *tab2 = (int *)malloc(sizeof(int)*nbCol*nbLin);
	for(int i = 0 ; i < nbLin ; i++){
		tab[i] = &tab2[i*nbCol];
	}
	return tab;
}

void inline MedianLinear::copy(int **tabA, int L, int C, int **tabB){
	for(int i = 0; i<L; i++){
		memcpy(tabB[i], tabA[i], C*sizeof(tabA[0]));
	}
}

void MedianLinear::freeH(int **tab){
	free(tab[0]);
	free(tab);
}

int MedianLinear::medianOfHisto(int *histo, int P, int numOfElems){
	int c =0;
	for(int i=0; i<P; i++){
		if(histo[i] != 0){
			c+=histo[i];
			if(c >= numOfElems/2){
				return i;
			}
		}
	}
	std::cerr<<"error searching median"<<std::endl;
	exit(EXIT_FAILURE);
}

void MedianLinear::removeHisto(int *histo, int P){
	for(int i =0; i<P; i++){
		currentHisto_[i] -= histo[i];
	}
}

void MedianLinear::addHisto(int *histo, int P){
	for(int i=0; i<P; i++){
		currentHisto_[i] += histo[i];
	}
}

void MedianLinear::printHisto(int *histo, int P){
	int tot=0;
	for(int i=0; i<P; i++){
		if(histo[i]!=0) {
			std::cout<<i<<": "<<histo[i]<<std::endl;
			tot+=histo[i];
		}
	}
	std::cout<<std::endl<<"Total:\t"<<tot<<std::endl;
	//TODO GET RID OF 19
	std::cout<<"Median:\t"<<medianOfHisto(histo, P, 19)<<std::endl<<std::endl;
}

void MedianLinear::medianFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int L, int C, int R, int P){
	if(R >= L/2){
		std::cerr<<"R is too big"<<std::endl;
		exit(EXIT_FAILURE);
	}

	uint8 *im_in = (uint8*)(frame_gray.data);
	uint8 *im_out = (uint8*)(frame_sobel.data);	

	uint8 *im_in0 = &im_in[0];
	uint8 *im_out0 = &im_out[0];
	
	int sizeOfElem = (R*2+1)*(R*2+1);

	for(int i=0; i<L; i++){
		memset(histos_[i], 0, sizeof(currentHisto_[0])*P);
		for(int j=0; j<R*2+1; j++){
			histos_[i][(int)im_in[i+j*L]] += 1;
		}
	}

	memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
	for(int i=0; i<R*2+1; i++){
		addHisto(histos_[i], P);
	}
	
	for(int i = 0 ; i < L*C; i+=1) {
		if(i%L == 0){
			memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
			for(int j=0; j<R*2+1; j++){
				addHisto(histos_[j], P);
			}

			*(im_out0) = *(im_in0);
		}
		if(i<R*L || i%L >= L-R-1 || i%L <= R+1 || i>L*C - R*L){
			*(im_out0) = *(im_in0);
		}
		else{
			histos_[i % L + R][*(im_in0 + R*L + R)] +=1;
			histos_[i % L + R][*(im_in0 - R*L + R)] -=1;

			addHisto(histos_[(i % L) + R], P);
			removeHisto(histos_[(i % L) - R], P);
			*(im_out0) = medianOfHisto(currentHisto_, P, sizeOfElem);
		}
		
	 	im_out0++;
	 	im_in0++;
	}
}