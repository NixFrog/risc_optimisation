#include <iostream>
#include <chrono>

/* Libraries OPENCV */
#include "highgui.h"
#include "cv.h"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

/* Own headers */
#include "Sobel.hpp"
#include "Median.hpp"
#include "MedianLinear.hpp"
#include "MedianLinearOpti.hpp"

Sobel sobel_;
Median *median_;
MedianLinear *medianLinear_;
MedianLinearOpti *medianLinearOpti_;

/*--------------- MAIN FUNCTION ---------------*/
int main(int argc, char const *argv[]){
	if(argc !=2){
		std::cout<<"Wrong number of parameters: ./prog <r>"<<std::endl;
		exit(EXIT_FAILURE);
	}
	int r = atoi(argv[1]);

	cv::VideoCapture cap(0);
	
	if(!cap.isOpened()){
		std::cerr << "Error capturing video flux"; return -1;
		return EXIT_FAILURE;
	}

	cv::Mat3b frame;
	cv::Mat frame_gray;
	cv::Mat frame_sobel;
	cv::Mat frame_median;
	char key = '0';

	cap.read(frame);
	cvtColor(frame, frame_gray, CV_BGR2GRAY);
	cvtColor(frame, frame_sobel, CV_BGR2GRAY);
	cvtColor(frame, frame_median, CV_BGR2GRAY);

	median_ = new Median(frame_gray.rows, 3);
	medianLinear_ = new MedianLinear(frame_gray.cols, 256);
	medianLinearOpti_ = new MedianLinearOpti(frame_gray.cols);


	std::chrono::high_resolution_clock::time_point start;
	std::chrono::duration<double> elapsedGray, elapsedSobel, elapsedMedian;
	auto startTotal = std::chrono::high_resolution_clock::now();

	// while(key!='q'){
	for(int i=0; i<100; i++){
		cap.read(frame);

		start = std::chrono::high_resolution_clock::now();
		cvtColor(frame, frame_gray, CV_BGR2GRAY);
		elapsedGray += (std::chrono::high_resolution_clock::now() - start);
		
		start = std::chrono::high_resolution_clock::now();
		// median_->medianFilter(frame_gray, frame_median, frame_gray.cols, frame_gray.rows, 15);
		// medianLinear_->medianFilter(frame_gray, frame_median, frame_sobel.cols, frame_sobel.rows, 10, 256);
		medianLinearOpti_->medianFilter(frame_gray, frame_median, frame_sobel.cols, frame_sobel.rows, r);
		elapsedMedian += (std::chrono::high_resolution_clock::now() - start);

		start = std::chrono::high_resolution_clock::now();
		sobel_.sobelFilter(frame_median, frame_sobel, frame_gray.cols, frame_gray.rows);
		elapsedSobel += (std::chrono::high_resolution_clock::now() - start);

		// resize(frame, frame, cv::Size(), 0.5, 0.5);
		// resize(frame_gray, frame_gray, cv::Size(), 0.5, 0.5);
		// resize(frame_sobel, frame_sobel, cv::Size(), 0.5, 0.5);
		
		imshow("Input",frame);  
		imshow("Output Gray",frame_gray);  
		imshow("Output Median",frame_median);  
		imshow("Output Sobel",frame_sobel);  

		key=cv::waitKey(5);
	}

	std::chrono::duration<double> elapsedTotal = std::chrono::high_resolution_clock::now() - startTotal;
	std::cout << elapsedTotal.count() << " s (Total)" << std::endl;
	std::cout << elapsedGray.count() << " s (Gray)" << std::endl;
	std::cout << elapsedSobel.count() << "s (Sobel)" << std::endl;
	std::cout << elapsedMedian.count() << " s (Median)" << std::endl;
	
	// Free memory
	cvDestroyWindow("Input");
	cvDestroyWindow("Output Gray");
	cvDestroyWindow("Output Sobel");
	delete median_;

	return EXIT_SUCCESS;
}

    