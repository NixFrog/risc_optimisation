#pragma once

#include <iostream>
#include <cmath>
#include <string>

#include "cv.h"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

// types definitions
typedef unsigned char uint8;

#define SWAP(a, b) { if ((a)>(b)) std::swap((a),(b)); }

class MedianLinear {
private:
	int **tab_;


public:
	MedianLinear(int C, int P);
	~MedianLinear();
	void medianFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int L, int C, int R, int P);
	
private:
	int **alloc(int L, int C);	
	void inline copy(int **tabA, int L, int C, int **tabB);
	void freeH(int **tab);
	int medianOfHisto(int *histo, int P, int sizeOfElem);
	void removeHisto(int *histo, int P);
	void addHisto(int *histo, int P);
	void printHisto(int *histo, int P);
	
private:
	int **histos_;
	int *currentHisto_;
};