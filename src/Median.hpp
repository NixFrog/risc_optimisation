#pragma once

#include <iostream>
#include <cmath>

#include "cv.h"
#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

// types definitions
typedef unsigned char uint8;

#define SWAP_PIXEL(a, b) { if ((a)>(b)) std::swap((a),(b)); }

class Median {
private:
	int **tab_;


public:
	~Median();
	Median(int nbCol, int nbLin);
	
	void copy(int **tabToCopy, int L, int C);

	void medianFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl, int KER);

	unsigned char mergeSort(unsigned char* tab, int beg, int end);

	void MedianFilter3(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl);
	void medianFilter5(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl);
	void medianFilter9(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl);
	void medianFilter25(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl);

	unsigned char inline medianMatrixApplication3(unsigned char &i10, unsigned char &i11, unsigned char &i12);
	unsigned char inline medianMatrixApplication5(unsigned char &i01, unsigned char &i10, unsigned char &i11, unsigned char &i12, unsigned char &i21);
	unsigned char inline medianMatrixApplication9(unsigned char &i00, unsigned char &i01, unsigned char &i02, unsigned char &i10, unsigned char &i11, unsigned char &i12, unsigned char &i20, unsigned char &i21, unsigned char &i22);
	unsigned char medianMatrixApplication25(unsigned char &i00, unsigned char &i01, unsigned char &i02, unsigned char &i03, unsigned char &i04, unsigned char &i10, unsigned char &i11, unsigned char &i12, unsigned char &i13, unsigned char &i14, unsigned char &i20, unsigned char &i21, unsigned char &i22, unsigned char &i23, unsigned char &i24, unsigned char &i30, unsigned char &i31, unsigned char &i32, unsigned char &i33, unsigned char &i34, unsigned char &i40, unsigned char &i41, unsigned char &i42, unsigned char &i43, unsigned char &i44);

	void inline sortThree(unsigned char *a, unsigned char *b, unsigned char *c);
	void inline sortFive(unsigned char *a, unsigned char *b, unsigned char *c, unsigned char *d, unsigned char *e);

};


