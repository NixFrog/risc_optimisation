#include "Median.hpp"


Median::Median(int rw, int cl){
	// tab_ = (int **)malloc(sizeof(int*)*rw);
	// int *tab2 = (int *)malloc(sizeof(int)*rw*cl);
	// for(int i = 0 ; i < rw ; i++){
	// 	tab_[i] = &tab2[i*cl];
	// }
}

Median::~Median(){
	// free(tab_[0]);
	// free(tab_);
}

void Median::copy(int **tabToCopy, int rw, int cl){
	for(int i = 0; i<rw; i++){
		memcpy(tab_[i], tabToCopy[i], cl*sizeof(tabToCopy[0]));
	}
}

unsigned char Median::mergeSort(unsigned char *tab, int beg, int end){

		
	if(beg==end)return -1;

	int mid = (beg+end)/2;
	mergeSort(tab,beg,mid);
	mergeSort(tab,mid+1,end);
	int i=beg, j=mid+1;
	int n=end-beg+1;
	unsigned char* tab2 = NULL;

	tab2 = (unsigned char*)malloc(sizeof(unsigned char)*n);

	if(tab2==NULL)
		exit(-1);

	for(int k=0;k<n;k++){
		if(j>end || (i<=mid && tab[i]<tab[j])){
			tab2[k]=tab[i];
			i++;
		}
		else{
			tab2[k]=tab[j];
			j++;
		}
	}
	
	for(int k=0, i=beg;k<n;k++,i++){
		tab[i]=tab2[k];
	}

	free(tab2);

	return tab[(end+1)/2];
}


void Median::medianFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl, int KER){
	unsigned char* im_in = (unsigned char*)(frame_gray.data); 
	unsigned char* im_out = (unsigned char*)(frame_sobel.data); 

	unsigned char* tmp = NULL;

	tmp = (unsigned char*)malloc(sizeof(unsigned char)*KER*KER);

	if(tmp==NULL)
		exit(-2);


	int max = rw*(cl-2) -2;
	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];
	
	
	for (int i = 0 ; i < max; i++) {


		im_out0++;
		
		if ((i+1) % rw != 0 && (i+2) % rw != 0) {
			for(int l=0; l<KER; l++){
				for(int j=0; j<KER; j++){
					tmp[j+l*KER]= *(im_in0 + l*rw + j);
				}
			}
			*(im_out0) = mergeSort(tmp,0,KER-1);
		}	

		if ((i+2) % rw != 0) { // exclude left and right borders
			for(int l=0; l<KER; l++){
				for(int j=0; j<KER; j++){
					tmp[j+l*KER]= *(im_in0 + l*rw + j);
				}
			}
			*(im_out0) = mergeSort(tmp,0,KER-1);}
		im_in0++;

	}

	free(tmp);
}

unsigned char inline Median::medianMatrixApplication3(unsigned char &i10, unsigned char &i11, unsigned char &i12){
	sortThree(&i10,&i11,&i12);
	return i11;
}
	
unsigned char inline Median::medianMatrixApplication5(unsigned char &i01, unsigned char &i10, unsigned char &i11, unsigned char &i12, unsigned char &i21){
	sortThree(&i01,&i11,&i21);	
	sortThree(&i10,&i11,&i12);
	return i11;
}

unsigned char Median::medianMatrixApplication9(unsigned char &i00, unsigned char &i01, unsigned char &i02, unsigned char &i10, unsigned char &i11, unsigned char &i12, unsigned char &i20, unsigned char &i21, unsigned char &i22){
	// Sort cols
	sortThree(&i00,&i10,&i20);
	sortThree(&i01,&i11,&i21);
	sortThree(&i02,&i12,&i22);

	// Sort lines
	sortThree(&i00,&i01,&i02);
	sortThree(&i10,&i11,&i12);
	sortThree(&i20,&i21,&i22);
	
	// Sort diagonal
	sortThree(&i02,&i11,&i20);
	
	return i11;
}

unsigned char Median::medianMatrixApplication25(unsigned char &i00, unsigned char &i01, unsigned char &i02, unsigned char &i03, unsigned char &i04, unsigned char &i10, unsigned char &i11, unsigned char &i12, unsigned char &i13, unsigned char &i14, unsigned char &i20, unsigned char &i21, unsigned char &i22, unsigned char &i23, unsigned char &i24, unsigned char &i30, unsigned char &i31, unsigned char &i32, unsigned char &i33, unsigned char &i34, unsigned char &i40, unsigned char &i41, unsigned char &i42, unsigned char &i43, unsigned char &i44){
	// Sort cols
	sortFive(&i00,&i10,&i20,&i30,&i40);
	sortFive(&i01,&i11,&i21,&i31,&i41);
	sortFive(&i02,&i12,&i22,&i32,&i42);
	sortFive(&i30,&i31,&i32,&i33,&i34);
	sortFive(&i40,&i41,&i42,&i43,&i44);

	// Sort lines
	sortFive(&i00,&i01,&i02,&i03,&i04);
	sortFive(&i10,&i11,&i12,&i13,&i14);
	sortFive(&i20,&i21,&i22,&i23,&i24);
	sortFive(&i30,&i31,&i32,&i33,&i34);
	sortFive(&i40,&i41,&i42,&i43,&i44);
	
	// Sort diagonal
	sortFive(&i04,&i13,&i22,&i31,&i40);
	
	return i22;
}

void inline Median::sortThree(unsigned char *a, unsigned char *b, unsigned char *c){
	SWAP_PIXEL(a,b);
	SWAP_PIXEL(b,c);
	SWAP_PIXEL(a,b);
}

void inline Median::sortFive(unsigned char *a, unsigned char *b, unsigned char *c, unsigned char *d, unsigned char *e){
	SWAP_PIXEL(a,b);
	SWAP_PIXEL(b,c);
	SWAP_PIXEL(c,d);
	SWAP_PIXEL(d,e);
	SWAP_PIXEL(a,b);
	SWAP_PIXEL(b,c);
	SWAP_PIXEL(c,d);
	SWAP_PIXEL(a,b);
	SWAP_PIXEL(b,c);
	SWAP_PIXEL(a,b);
}

void Median::MedianFilter3(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl){
	unsigned char *im_in = (unsigned char*)(frame_gray.data);
	unsigned char *im_out = (unsigned char*)(frame_sobel.data);

	int max = rw*(cl-2) -2;
	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];
	
	int rw_sum_two = rw + 2;

	for (int i = 0 ; i < max; i+=8) {

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication3(
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two)
			);
		}
		im_in0++;
	}
}

void Median::medianFilter5(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl){
	unsigned char *im_in = (unsigned char*)(frame_gray.data);
	unsigned char *im_out = (unsigned char*)(frame_sobel.data);

	int max = rw*(cl-2) -2;
	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];

	int rw_double = rw<<1;
	int rw_double_sum_one = rw_double +1;
	int rw_sum_two = rw + 2;
	
	for (int i = 0 ; i < max; i+=8) {

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication5(
											*(im_in0 +1),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
									 		*(im_in0 + rw_double_sum_one)
			);
		}
		im_in0++;
	}
}

void Median::medianFilter9(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl){
	unsigned char *im_in = (unsigned char*)(frame_gray.data);
	unsigned char *im_out = (unsigned char*)(frame_sobel.data);

	int max = rw*(cl-2) -2;
	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];
	
	int rw_double = rw<<1;
	int rw_double_sum_one = rw_double +1;
	int rw_sum_two = rw + 2;
	int rw_double_sum_two = rw_double +2;

	for (int i = 0 ; i < max; i+=8) {

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication9(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
	}
}

void Median::medianFilter25(cv::Mat& frame_gray, cv::Mat& frame_sobel, int rw, int cl){
	unsigned char *im_in = (unsigned char*)(frame_gray.data);
	unsigned char *im_out = (unsigned char*)(frame_sobel.data);

	int max = rw*(cl-2) -2;
	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];
	
	for (int i = 0 ; i < max; i+=8) {

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)					
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)			
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = medianMatrixApplication25(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 					*(im_in0+3),					*(im_in0+4),
				*(im_in0 + rw), 			*(im_in0 + rw +1),				*(im_in0 + rw +2),				*(im_in0 + rw +3),				*(im_in0 + rw +4),
				*(im_in0 + 2*rw), 			*(im_in0 + 2*rw +1), 			*(im_in0 + 2*rw +2),			*(im_in0 + 2*rw +3),			*(im_in0 + 2*rw +4),	
				*(im_in0 + 3*rw), 			*(im_in0 + 3*rw +1), 			*(im_in0 + 3*rw +2),			*(im_in0 + 3*rw +3),			*(im_in0 + 3*rw +4),			
				*(im_in0 + 4*rw), 			*(im_in0 + 4*rw +1), 			*(im_in0 + 4*rw +2),			*(im_in0 + 4*rw +3),			*(im_in0 + 4*rw +4)
			);
		}
		im_in0++;
	}
}
