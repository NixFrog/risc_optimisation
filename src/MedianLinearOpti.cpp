#include "MedianLinearOpti.hpp"

MedianLinearOpti::MedianLinearOpti(int C){
	histos_ = alloc(C, P);
	currentHisto_ = (int *)malloc(sizeof(int)*P);
}

MedianLinearOpti::~MedianLinearOpti(){
	freeH(histos_);
	free(currentHisto_);
}

inline int **MedianLinearOpti::alloc(int nbLin, int nbCol){
	int **tab = (int **)malloc(sizeof(int*)*nbLin);
	int *tab2 = (int *)malloc(sizeof(int)*nbCol*nbLin);
	for(int i = 0 ; i < nbLin ; i++){
		tab[i] = &tab2[i*nbCol];
	}
	return tab;
}

inline void MedianLinearOpti::freeH(int **tab){
	free(tab[0]);
	free(tab);
}

inline int MedianLinearOpti::medianOfHisto(int *histo, int numOfElems){
	int c =0;

	int *histo0 = &histo[0];
	
	for(int i=0; i<P; i++){
		if(*(histo0) != 0){
			c+=*(histo0);
			if(c >= (numOfElems>>2)){
				return i;
			}
		}
		histo0 ++;
	}
	std::cerr<<"error searching median"<<std::endl;
	exit(EXIT_FAILURE);
}

inline void MedianLinearOpti::removeHisto(int *histo){
	for(int i =0; i<P; i++){
		currentHisto_[i] -= histo[i];
	}
}

inline void MedianLinearOpti::addHisto(int *histo){
	for(int i=0; i<P; i++){
		currentHisto_[i] += histo[i];
	}
}

inline void MedianLinearOpti::printHisto(int *histo){
	int tot=0;
	for(int i=0; i<P; i++){
		if(histo[i]!=0) {
			std::cout<<i<<": "<<histo[i]<<std::endl;
			tot+=histo[i];
		}
	}
	std::cout<<std::endl<<"Total:\t"<<tot<<std::endl;
}

void MedianLinearOpti::medianFilter(cv::Mat& frame_gray, cv::Mat& frame_sobel, int L, int C, int R){
	if(R >= L/2){
		std::cerr<<"R is too big"<<std::endl;
		exit(EXIT_FAILURE);
	}

	unsigned char *im_in = (unsigned char*)(frame_gray.data);
	unsigned char *im_out = (unsigned char*)(frame_sobel.data);	

	unsigned char *im_in0 = &im_in[0];
	unsigned char *im_out0 = &im_out[0];
	
	int R21 = (R<<1)+1;
	int sizeOfElem = (R21)*(R21);
	int rl = R*L;

	for(int i=0; i<L; i++){
		memset(histos_[i], 0, sizeof(histos_[0])*P);
		for(int j=0; j<R21; j++){
			histos_[i][(int)im_in[i+j*L]] += 1;
		}
	}

	memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
	for(int i=0; i<R21; i++){
		addHisto(histos_[i]);
	}

	// #pragma omp parallel for num_threads(4)
	for(int i = 0 ; i < L*C; i+=4) {
		int il = i%L;
		if(il == 0){
			memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
			for(int j=0;j<R;j++){
				histos_[j][*(im_in0 + rl)] +=1;
				histos_[j][*(im_in0 - rl)] -=1;
				addHisto(histos_[j]);
			}
		}
		if(i<rl || il >= L-R-1 || il <= R+1 || i>L*C - rl){
			*(im_out0) = *(im_in0);
		}
		else{
			histos_[il + R][*(im_in0 + rl + R)] +=1;
			histos_[il + R][*(im_in0 - rl + R)] -=1;

			addHisto(histos_[il + R]);
			removeHisto(histos_[il - R]);
			*(im_out0) = medianOfHisto(currentHisto_, sizeOfElem);
		}
		
	 	im_out0++;
	 	im_in0++;

	 	il = (i+1)%L;
		if(il == 0){
			memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
			for(int j=0;j<R;j++){
				histos_[j][*(im_in0 + rl)] +=1;
				histos_[j][*(im_in0 - rl)] -=1;
				addHisto(histos_[j]);
			}
		}
		if(i<rl || il >= L-R-1 || il <= R+1 || i>L*C - rl){
			*(im_out0) = *(im_in0);
		}
		else{
			histos_[il + R][*(im_in0 + rl + R)] +=1;
			histos_[il + R][*(im_in0 - rl + R)] -=1;

			addHisto(histos_[il + R]);
			removeHisto(histos_[il - R]);
			*(im_out0) = medianOfHisto(currentHisto_, sizeOfElem);
		}
		
	 	im_out0++;
	 	im_in0++;

		il = (i+2)%L;
		if(il == 0){
			memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
			for(int j=0;j<R;j++){
				histos_[j][*(im_in0 + rl)] +=1;
				histos_[j][*(im_in0 - rl)] -=1;
				addHisto(histos_[j]);
			}
		}
		if(i<rl || il >= L-R-1 || il <= R+1 || i>L*C - rl){
			*(im_out0) = *(im_in0);
		}
		else{
			histos_[il + R][*(im_in0 + rl + R)] +=1;
			histos_[il + R][*(im_in0 - rl + R)] -=1;

			addHisto(histos_[il + R]);
			removeHisto(histos_[il - R]);
			*(im_out0) = medianOfHisto(currentHisto_, sizeOfElem);
		}
		
	 	im_out0++;
	 	im_in0++;

	 	il = (i+3)%L;
		if(il == 0){
			memset(currentHisto_, 0, sizeof(currentHisto_[0])*P);
			for(int j=0;j<R;j++){
				histos_[j][*(im_in0 + rl)] +=1;
				histos_[j][*(im_in0 - rl)] -=1;
				addHisto(histos_[j]);
			}
		}
		if(i<rl || il >= L-R-1 || il <= R+1 || i>L*C - rl){
			*(im_out0) = *(im_in0);
		}
		else{
			histos_[il + R][*(im_in0 + rl + R)] +=1;
			histos_[il + R][*(im_in0 - rl + R)] -=1;

			addHisto(histos_[il + R]);
			removeHisto(histos_[il - R]);
			*(im_out0) = medianOfHisto(currentHisto_, sizeOfElem);
		}
		
	 	im_out0++;
	 	im_in0++;


	}
}