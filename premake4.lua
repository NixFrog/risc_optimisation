solution "projet_optimisation"
	configurations ""
	project "video_flux_optimisation"
	kind "ConsoleApp"
	language "C++"
	files({ "src/*.h", "src/*.cpp" })
	--includedirs({"/", "includes"})

	flags({"Symbols", "ExtraWarnings"})
	links({"gomp"})
	--libdirs({"Driver/"})

	buildoptions({"-std=c++0x", "`pkg-config --cflags opencv`", "-Ofast", "-march=native", "-mtune=native", "-pg", "-fopenmp"})
	linkoptions({"-std=c++0x", "-lpthread", "-lm", "`pkg-config --libs opencv`", "-pg", "-lm", "-fopenmp"})
